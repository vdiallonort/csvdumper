﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using CsvDumper;
using Microsoft.SqlServer.Management.Smo;
using System.Collections.ObjectModel;
using System.Threading;

namespace ClientCsvDumper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TableDumper td;
        Database selected_db;
        Server s;
        Schema selected_schema;
        int thread_number = 0;
        public MainWindow()
        {
            InitializeComponent();
            td = new TableDumper();
            this.schema_name.Visibility = Visibility.Hidden;
            this.table_name.Visibility = Visibility.Hidden;
            this.schema_label.Visibility = Visibility.Hidden;
            this.tabel_label.Visibility = Visibility.Hidden;
            this.query_textbox.Visibility = Visibility.Hidden;
            

        }

        async void go_Click(object sender, RoutedEventArgs e)
        {
            String database_name;
            String schema_name;
            String output_file_name;

            database_name = this.database_name.SelectedItem.ToString();
            
            output_file_name = this.output_file_name.Text;

            SqlConnection conn = new SqlConnection("Data Source="+this.server_name.Text+";Initial Catalog="+"MASTER"+";Integrated Security=True");
            if (this.query_button.IsChecked.Value) {
                String query_text;
                query_text = this.query_textbox.Text;
                thread_number++;
                await Task.Run(() => {
                    UpdateWindows("task " + thread_number.ToString() + " started");
                    AsyncDumpQueryToFile(conn, database_name, query_text, output_file_name);
                });


            }
            else {
                schema_name = this.schema_name.SelectedItem.ToString();
                String table_name;
                table_name = this.table_name.SelectedItem.ToString();
                thread_number++;
                await Task.Run(() => {UpdateWindows("task " + thread_number.ToString() + " started");
                    AsyncDumpTableToFile(conn, database_name, schema_name, table_name, output_file_name);
                });
               
             }
            
        } 

        private void server_name_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void database_name_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            s = new Microsoft.SqlServer.Management.Smo.Server(this.server_name.Text);
            List<string> list_databases = new List<string>();

            foreach (Database d in s.Databases)
            {
                list_databases.Add(d.Name);

            }
            this.database_name.ItemsSource = list_databases;
        }

        private void database_name_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ObservableCollection<string> list = new ObservableCollection<string>();
            s = new Microsoft.SqlServer.Management.Smo.Server(this.server_name.Text);


            foreach (Database d in s.Databases)
            {
                list.Add(d.Name);

            }
            this.database_name.ItemsSource = list;


        }

        private void database_name_DropDownOpened(object sender, EventArgs e)
        {

            ObservableCollection<string> list = new ObservableCollection<string>();
            Server s = new Microsoft.SqlServer.Management.Smo.Server(this.server_name.Text);


            foreach (Database d in s.Databases)
            {
                list.Add(d.Name);

            }
            this.database_name.ItemsSource = list;
        }

        private void schema_name_DropDownOpened(object sender, EventArgs e)
        {
            ObservableCollection<string> list = new ObservableCollection<string>();
            Server s = new Microsoft.SqlServer.Management.Smo.Server(this.server_name.Text);
            
            foreach( Database db in s.Databases)
            {
                if (db.Name == this.database_name.SelectedItem.ToString())
                {
                    selected_db = db;
                    break;

                }


            }

            foreach (Schema sch in selected_db.Schemas) {

                list.Add(sch.Name);
            }
            this.schema_name.ItemsSource = list;


        }
        async Task AsyncDumpTableToFile( SqlConnection connection, string database_name, string schema_name, string tableName, string destinationFile) {
           // gui.UpdateWindows("Work Started");
            TableDumper ts = new TableDumper();
            ts.DumpTableToFile(connection, database_name, schema_name, tableName, destinationFile);

          //  gui.UpdateWindows("Done");
        }
        async Task AsyncDumpQueryToFile( SqlConnection connection, string database_name, string query, string destinationFile)
        {
            //gui.UpdateWindows("Work Started");
            TableDumper ts = new TableDumper();
            ts.DumpQueryToFile( connection,  database_name,  query,  destinationFile);

           // gui.UpdateWindows("Done");
        }

        void UpdateWindows(String text) {

            Dispatcher.Invoke(() => { status.Text += text; });
        }

        private void table_name_DropDownOpened(object sender, EventArgs e)
        {
            ObservableCollection<string> list = new ObservableCollection<string>();
            foreach (Schema sch in selected_db.Schemas)
            {

                if (sch.Name == this.schema_name.SelectedItem.ToString()) {
                    selected_schema = sch;
                    break;
                }
                
            }
            foreach (Microsoft.SqlServer.Management.Smo.Table t in selected_db.Tables) {
                if (t.Schema.ToString() == this.schema_name.SelectedItem.ToString()) {
                    list.Add(t.Name);
                    
                }


            }
            foreach (Microsoft.SqlServer.Management.Smo.View v in selected_db.Views) {
                if (v.Schema.ToString() == this.schema_name.SelectedItem.ToString()) {
                    list.Add(v.Name);
                    
                }
                this.table_name.ItemsSource = list;

            }
            
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.query_button.IsChecked = false;
            this.query_textbox.Visibility = Visibility.Visible;
            this.schema_name.Visibility = Visibility.Visible;
            this.table_name.Visibility = Visibility.Visible;
            this.schema_label.Visibility = Visibility.Visible;
            this.tabel_label.Visibility = Visibility.Visible;
            this.query_textbox.Visibility = Visibility.Hidden;

        }

        private void query_button_Checked(object sender, RoutedEventArgs e)
        {
            this.Table_view_button.IsChecked = false;
            this.query_textbox.Visibility = Visibility.Visible;
            this.schema_name.Visibility = Visibility.Hidden;
            this.table_name.Visibility = Visibility.Hidden;
            this.schema_label.Visibility = Visibility.Hidden;
            this.tabel_label.Visibility = Visibility.Hidden;

        }
    }
}
