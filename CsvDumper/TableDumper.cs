﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
namespace CsvDumper
{
    public class TableDumper
    {
        Dictionary<int, Func<Object, String>> list_type;
        Dictionary<int, String> list_field_type;
        public TableDumper() {
            list_field_type = new Dictionary<int, string>();
        }
        public void DumpTableToFile(SqlConnection connection,string database_name, string schema_name, string tableName, string destinationFile)
        {
            Dictionary<String, String> list_value = new Dictionary<string, string>();
            list_value.Add("database_name", database_name);
            list_value.Add("schema_name", schema_name);
            list_value.Add("tableName", tableName);
            list_value.Add("destinationFile", destinationFile);

            DumpToFile(connection, list_value);
        }
        public void DumpQueryToFile(SqlConnection connection, string database_name, string query, string destinationFile)
        {
            Dictionary<String, String> list_value = new Dictionary<string, string>();
            list_value.Add("database_name", database_name);
            list_value.Add("query", query);
            list_value.Add("destinationFile", destinationFile);

            DumpToFile(connection, list_value);
        }


        public void DumpToFile(SqlConnection connection, Dictionary<String,String> list_value)
        {
            String sql_command;

            if (list_value.ContainsKey("query"))
            {

                sql_command = list_value.SingleOrDefault(x => x.Key == "query").Value;

            }
            else {

                sql_command = "select * from [" + list_value["schema_name"] + "].[" + list_value["tableName"] + "]";
            }

            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();

            }
            connection.ChangeDatabase(list_value["database_name"]);
            // using (var command = new SqlCommand(sql_command, connection))
            SqlCommand command = new SqlCommand(sql_command, connection);
            command.CommandTimeout = 0;

            using (var reader = command.ExecuteReader())
           
            using (var outFile = File.CreateText(list_value["destinationFile"]))
            {
                string[] columnNames = GetColumnNames(reader).ToArray();
                int numFields = columnNames.Length;
                outFile.WriteLine(string.Join(",", columnNames));
                if (reader.HasRows)
                {
                    // List all the datatype
                    buidList(reader, numFields);

                    while (reader.Read())
                    {
                        //for (int i = 0; i < numFields; i++) {
                        //    string[] columnValues = new string[numFields];



                        //}


                        string[] columnValues =
                            Enumerable.Range(0, numFields)
                                      .Select(i => list_type[i].Invoke(reader.GetValue(i)))
                                      //.Select(field => util.RemoveSpecialCharacters(field))
                                      .ToArray();
                        outFile.WriteLine(string.Join(",", columnValues));

                    }
                }
            }
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            if (command != null) {
                ((IDisposable)command).Dispose();
            }
        }

        private IEnumerable<string> GetColumnNames(IDataReader reader)
        {
            foreach (DataRow row in reader.GetSchemaTable().Rows)
            {
                yield return (string)row["ColumnName"];
            }
        }
        private string varchar(System.Object val) {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + util.RemoveSpecialCharacters(val.ToString()) + "\"";
                }
                else
                {
                    return val.ToString();
                }
            }else { return "NULL"; }
        }

        private string charm(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + util.RemoveSpecialCharacters(val.ToString()) + "\"";
                }
                else
                {
                    return val.ToString();
                }
            }else { return "NULL"; }
        }

        private string nvarchar(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + util.RemoveSpecialCharacters(val.ToString()) + "\"";
                }
                else
                {
                    return val.ToString();
                }
            }
            else
            {
                return "NULL";

            }
        }
        private string intm(System.Object val)
        {
            if (!(val is DBNull)){ 
            if (Properties.Settings.Default.DoubleQuoteEnclosure)
            {

                return "\"" + Convert.ToInt32(val).ToString() + "\"";
            }
            else
            {
                return Convert.ToInt32(val).ToString();
            }
            }
            else {
                return "NULL";
                    }
        }
        private string money(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToDecimal(val).ToString() + "\"";
                }
                else
                {
                    return Convert.ToDecimal(val).ToString();
                }
            }
            else
            {
                return "NULL";


            }
        }
        private string date(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToDateTime(val).ToString(Properties.Settings.Default.DateFormat) + "\"";
                }
                else
                {
                    return Convert.ToDateTime(val).ToString(Properties.Settings.Default.DateFormat);
                }
            }
            else
            {
                return "NULL";

            }
        }
        private string datetime(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToDateTime(val).ToString(Properties.Settings.Default.TimeStampFormat) + "\"";
                }
                else
                {
                    return Convert.ToDateTime(val).ToString(Properties.Settings.Default.TimeStampFormat);
                }
            }
            else
            {
                return "NULL";


            }
        }

        private string datetime2(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToDateTime(val).ToString(Properties.Settings.Default.TimeStampFormat) + "\"";
                }
                else
                {
                    return Convert.ToDateTime(val).ToString(Properties.Settings.Default.TimeStampFormat);
                }
            }
            else
            {
                return "NULL";


            }
        }
        private string bigint(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToInt64(val).ToString() + "\"";
                }
                else
                {
                    return Convert.ToInt64(val).ToString();
                }
            }
            else
            {
                return "NULL";

            }
        }
        private string binary(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + val.ToString() + "\"";
                }
                else
                {
                    return val.ToString();
                }
            }
            else
            {
                return "NULL";


            }
        }
        private string bit(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToBoolean(val).ToString() + "\"";
                }
                else
                {
                    return Convert.ToBoolean(val).ToString();
                }
            }
            else {
                return "NULL";
            }
        }
        private string decimalm(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToDecimal(val).ToString() + "\"";
                }
                else
                {
                    return Convert.ToDecimal(val).ToString();
                }
            }
            else { return "NULL"; }
        }
        private string floatm(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToDouble(val).ToString() + "\"";
                }
                else
                {
                    return Convert.ToDouble(val).ToString().ToString();
                }
            }
            else
            {
                return "NULL";

            }
        }
        private string nchar(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + val.ToString() + "\"";
                }
                else
                {
                    return val.ToString().ToString();
                }
            }else
            {
                return "NULL";

            }
        }
        private string ntext(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + val.ToString() + "\"";
                }
                else
                {
                    return val.ToString().ToString();
                }
            }
            else
            {
                return "NULL";

            }
        }
        private string real(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToSingle(val).ToString() + "\"";
                }
                else
                {
                    return Convert.ToSingle(val).ToString();
                }
            }
            else {

                return "NULL";
            }
        }
        private string rowversion(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + (val).ToString() + "\"";
                }
                else
                {
                    return (val).ToString();
                }
            }else
            {
                return "NULL";

            }
        }
        private string smalldatetime(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToDateTime(val).ToString(Properties.Settings.Default.DateFormat).ToString() + "\"";
                }
                else
                {
                    return Convert.ToDateTime(val).ToString(Properties.Settings.Default.DateFormat).ToString();
                }
            }
            else
            {

                return "NULL";

            }
        }
        private string smallint(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + val.ToString() + "\"";
                }
                else
                {
                    return val.ToString();
                }
            }else
            {
                return "NULL";

            }
        }
        private string smallmoney(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + Convert.ToDecimal(val).ToString() + "\"";
                }
                else
                {
                    return Convert.ToDecimal(val).ToString();
                }
            }
            else
            {
                return "NULL";

            }
        }
        private string time(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + (val).ToString() + "\"";
                }
                else
                {
                    return (val).ToString();
                }
            }
            else {
                return "NULL";
            }
        }
        private string tinyint(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + (val).ToString() + "\"";
                }
                else
                {
                    return (val).ToString();
                }
            }
            else
            {
                return "NULL";

            }
        }
        private string uniqueidentifier(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + (val).ToString() + "\"";
                }
                else
                {
                    return (val).ToString();
                }
            }
            else
            {
                return "NULL";

            }
        }
        private string varbinary(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + (val).ToString() + "\"";
                }
                else
                {
                    return (val).ToString();
                }
            }
            else {

                return "NULL";
            }
        }
        private string xml(System.Object val)
        {
            if (!(val is DBNull))
            {
                if (Properties.Settings.Default.DoubleQuoteEnclosure)
                {

                    return "\"" + (val).ToString() + "\"";
                }
                else
                {
                    return (val).ToString();
                }
            }
            else
            {
                return "NULL";

            }
        }


        public  string ToReadableString( TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}{3}",
                span.Duration().Days > 0 ? string.Format("{0:0} days, ", span.Days) : string.Empty,
                span.Duration().Hours > 0 ? string.Format("{0:0} hours, ", span.Hours) : string.Empty,
                span.Duration().Minutes > 0 ? string.Format("{0:0} minutes, ", span.Minutes) : string.Empty,
                span.Duration().Seconds > 0 ? string.Format("{0:0} seconds", span.Seconds) : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 seconds";

            return formatted;
        }




        private void buidList(SqlDataReader reader,int numFields) {
            list_type = new Dictionary<int, Func<Object, String>>();
            for (int i = 0; i < numFields; i++)
            {
                list_field_type.Add(i, reader.GetDataTypeName(i));
                switch (reader.GetDataTypeName(i))
                {
                    case "varchar":
                        list_type.Add(i, varchar);
                        break;
                    case "nvarchar":
                        list_type.Add(i, nvarchar);
                        break;
                    case "char":
                        list_type.Add(i, charm);
                        break;
                    case "int":
                        list_type.Add(i, intm);
                        break;
                    case "money":
                        list_type.Add(i, money);
                        break;
                    case "date":
                        list_type.Add(i, date);
                        break;
                    case "datetime":
                        list_type.Add(i,datetime);
                        break;
                    case "datetime2":
                        list_type.Add(i, datetime2);
                        break;
                    case "bigint":
                        list_type.Add(i, bigint);
                        break;
                    case "binary":
                        list_type.Add(i,binary);
                        break;
                    case "bit":
                        list_type.Add(i,bit);
                        break;
                    case "decimal":
                        list_type.Add(i, decimalm);
                        break;
                    case "floatm":
                        list_type.Add(i, floatm);
                        break;
                    case "nchar":
                        list_type.Add(i, nchar);
                        break;
                    case "ntext":
                        list_type.Add(i, ntext);
                        break;
                    case "real":
                        list_type.Add(i, real);
                        break;
                    case "rowversion":
                        list_type.Add(i,rowversion);
                        break;
                    case "smalldatetime":
                        list_type.Add(i,smalldatetime);
                        break;
                    case "smallint":
                        list_type.Add(i, smallint);
                        break;
                    case "smallmoney":
                        list_type.Add(i, smallmoney);
                        break;
                    case "time":
                        list_type.Add(i, time);
                        break;
                    case "tinyint":
                        list_type.Add(i,tinyint);
                        break;
                    case "uniqueidentifier":
                        list_type.Add(i, uniqueidentifier);
                        break;
                    case "varbinary":
                        list_type.Add(i, varbinary);
                        break;
                    case "xml":
                        list_type.Add(i,xml);
                        break;

                }

              
            }
            

        }

    }
}